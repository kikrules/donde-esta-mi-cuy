package com.kikappsmx.dondeestamicuy;

import android.os.Handler;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CuysCreator {

    public final static String GEO_FIRE = "geoFire";
    public final static String CUYS = "cuys";
    private final static double MIN = -0.0005;
    private final static double MAX = 0.0005;
    private final static int DELAY = 5000;

    public static void create() {
        for (int i = 0; i < 30; i++) {
            Cuy cuy = new Cuy();
            cuy.id = String.valueOf(i);
            updateLocation(cuy);
        }
    }

    private static void updateLocation(Cuy cuy) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child(CUYS).child(cuy.id).setValue(cuy, (databaseError, databaseReference) -> new Handler().postDelayed(() -> {
            GeoFire geoFire = new GeoFire(reference.child(GEO_FIRE));
            geoFire.setLocation(cuy.id, new GeoLocation(cuy.latitude, cuy.longitude), (key, error) -> {
                cuy.latitude += MIN + (Math.random() * (MAX - MIN));
                cuy.longitude += +MIN + (Math.random() * (MAX - MIN));
                updateLocation(cuy);
            });
        }, DELAY));
    }
}
