package com.kikappsmx.dondeestamicuy;

import android.app.Application;

public class CuyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CuysCreator.create();
    }
}
