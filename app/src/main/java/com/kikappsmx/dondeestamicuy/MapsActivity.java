package com.kikappsmx.dondeestamicuy;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
    private List<Marker> markers = new ArrayList<>();
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Cuy.LATITUDE, Cuy.LONGITUDE), 17));
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        addMarkers();
        updateMarkers();
    }

    private void addMarkers() {
        LatLng latLng = new LatLng(Cuy.LATITUDE, Cuy.LONGITUDE);
        for (int i = 0; i < 30; i++) {
            MarkerOptions markerOptions = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon))
                    .title(String.valueOf(i))
                    .position(latLng)
                    .draggable(false);
            markers.add(googleMap.addMarker(markerOptions));
        }
    }

    private void updateMarkers() {
        GeoFire geoFire = new GeoFire(reference.child(CuysCreator.GEO_FIRE));
        GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(Cuy.LATITUDE, Cuy.LONGITUDE), 0.1);
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                markers.get(Integer.parseInt(key)).setVisible(true);
            }

            @Override
            public void onKeyExited(String key) {
                markers.get(Integer.parseInt(key)).setVisible(false);
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                reference.child(CuysCreator.CUYS).child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Cuy cuy = dataSnapshot.getValue(Cuy.class);
                        if (cuy != null) {
                            LatLng latLng = new LatLng(cuy.latitude, cuy.longitude);
                            markers.get(Integer.parseInt(key)).setPosition(latLng);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }
}
